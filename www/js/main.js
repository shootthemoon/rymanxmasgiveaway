var formUrl = '/ajax?type=entry';

$(document).ready(function(){
	$('#entry-form').submit(function(e){
		e.preventDefault();
		// remove existing errors & msgs
		$('div.field-msg').slideUp();
		$('div.field-wrap').removeClass('error');
		
		validForm = true;
		
		$formInputs = $('#entry-form :input').filter(':not(.submit)');
		
		$formInputs.each(function(i,e){
			thisValue = $(e).val();
			if($(this).hasClass('required')){
				thisValue = $.trim(thisValue);
				if(thisValue == ''){
					validForm = false;
					$(this).parent('div').addClass('error');
					$(this).siblings('div.field-msg').html('This is a required field.').slideDown();
				}
			}
		});
		
		
		
		if(validForm){
			// we appear to have a valid submission. Now POST
			
			$.ajax({
				type: "POST",
				url: formUrl,
				data: $formInputs.serialize(),
				success: function(data){
					if(data.response){
						$('#app-form').slideUp({
							duration: 1400,
							complete:function(){
								$('#app-form').html('');
								$('<h2/>').html('Ooops there appears to be a problem with your entry.').appendTo('#app-form');
								$('<p/>').html('Thanks for letting us know you had a problem. We will be in touch soon.').appendTo('#app-form');
								$('<p/>').html('Find out more about the Ryman Countdown to Christmas Celebrations <a href="http://www.ryman.co.uk/countdown">here</a>.').appendTo('#app-form');
								
								$('#app-form').slideDown();
							}
						});
					}
					else if(data.voucherRedeemed){
						$('#app-form').slideUp({
							duration: 1400,
							complete:function(){
								
								formUrl = '/ajax?type=error';
								
								$('div.form-right').hide();
								$('div.form-left').hide();
								
								$('#entry-copy').remove();
								
								$('#app-form').children().filter(':not(form)').remove();
								
								$('<h2/>').html('Ooops there appears to be a problem with your entry.').prependTo('#app-form');
								$('<p/>').html('If you would like us to take a look at this please press the submit button to send your contact details to our customer services team, who will review and arrange to get a new entry code to enter the draw.  Please note there is only one entry per person and per code see <a href="/terms-and-conditions">terms and conditions</a> for more details.').insertAfter('h2');
								
								$('#app-form').slideDown();
							}
						});
					}
					else if(data.formValid){
						$('#app-form').slideUp({
							duration: 1400,
							complete:function(){
								$('#app-form').html('');
								$('<h2/>').html('Thanks for your submission, your entry has been saved.').appendTo('#app-form');
								$('<p/>').html('Thank you for entering the Ryman 120 years Celebration Prize Draw the winners will be drawn on Christmas Eve!').appendTo('#app-form');
								$('<p/>').html('Find out more about the Ryman Countdown to Christmas Celebrations <a href="http://www.ryman.co.uk/countdown">here</a>.').appendTo('#app-form');
								$('#app-form').slideDown();
							}
						});
					}
					else{
						Recaptcha.reload();
					//	alert('Sorry but there were errors in your form.\n\n\Please correct the errors and try again.');
					 
						$.each(data.errors, function(field, error){
							$('#'+field).parent('div').addClass('error');
							$('#'+field).siblings('div.field-msg').html(error).slideDown();
						});
					}
				},
				dataType: 'json'
			});
		}
		else{
			Recaptcha.reload();
			alert('Sorry please complete all required fields and try again.');
		}
	});
});