<?php
session_start();

switch( $_SERVER['HTTP_HOST'] ){
	
	// development environment
	case 'rymanxmasgiveaway.local':
		// set our environment
		define( 'APP_ENVIRONMENT', 'DEVELOPMENT' );
		
		// error reporting
		error_reporting( E_ALL );
		ini_set( "display_errors", 1 );
	break;
	
	// staging environment
	case 'rymanxmasgiveaway.stmpreview.co.uk':
		define( 'APP_ENVIRONMENT', 'STAGING' );
	
		error_reporting( E_ERROR );
		ini_set( "display_errors", 1 );
	break;
		
	// live environment
	default:
		define( 'APP_ENVIRONMENT', 'PRODUCTION' );
		
		error_reporting( E_ERROR );
		ini_set( "display_errors", 0 );
	break;
}

// set some basic paths
define( 'DOC_ROOT', $_SERVER['DOCUMENT_ROOT'] );
define( 'BASE_URL', 'http://'.$_SERVER['SERVER_NAME'] );
define( 'INCLUDES', DOC_ROOT . '/_includes' );
$url = preg_replace('/\?.*/', '', $_SERVER['REQUEST_URI']);

$uriPartsFull = rtrim( $url, '/' );

$uriParts = explode( "/", $uriPartsFull );

// go ahead and load our controller
$controller = ( isset( $uriParts[1] ) && $uriParts[1] != '' ) ? $uriParts[1] : "index" ;

$controllerPath = INCLUDES."/controllers/{$controller}.php";

if ( file_exists( $controllerPath ) ){
	include( $controllerPath );
}

else{
	$fatalError = 404;

	die( $controller . ' controller not found' );
}
?>