<?php
/*
 * File to contain our validation functions
 */

function isValidTitle( $title = '' ){
	return ( $title != '' && preg_match( '/(mr|mrs|miss|ms|dr|rev|prof)$/', $title ) ) ? true : false ;
}

function isValidNumber( $number = '' ){
	return ( $number != '' && preg_match( '/^[0-9\.\-]+$/', $number ) ) ? true : false ;
}

function isValidDecimal( $number = '' ){
	return ( $number != '' && preg_match( '/^\s*[+\-]?(?:\d+(?:\.\d*)?|\.\d+)\s*$/', $number ) ) ? true : false ;
}

function isValidTime( $time = '' ){
	return ( $time != '' && preg_match( '/^(([0-1]?[0-9])|([2][0-3])):([0-5]?[0-9])(:([0-5]?[0-9]))?$/', $time ) ) ? true : false ;
}

function isValidName( $name = '' ){
	return ( $name != '' && preg_match( '/^[a-zA-Z \-\']+$/', $name ) ) ? true : false ;
}

function isValidDescription( $description = '' ){
	return ( $description != '' && preg_match( '/^[a-zA-Z0-9 \-\'\,\.\�\$\%\&]+$/', $description ) ) ? true : false ;
}

function isValidAlphaNumeric( $string = '' ){
	return ( $string != '' && preg_match( '/^[a-zA-Z0-9]+$/', $string ) ) ? true : false ;
}

function isValidCompanyName( $companyName = '' ){
	return ( $companyName != '' && preg_match( '/^[a-zA-Z0-9 \.\,\-\']+$/', $companyName ) ) ? true : false ;
}

function isValidMessage( $message = '' ){
	return ( $message != '' && preg_match( '/^[0-9a-zA-Z \%\�]+$/', $message ) ) ? true : false ;
}

function isValidPhone( $phone = '' ){
	return ( $phone != '' && preg_match( '/^[0-9 \-\+\(\)]+$/', $phone ) ) ? true : false ;
}

function isValidHouseName( $houseName = '' ){
	return ( $houseName != '' && preg_match( '/^[0-9a-zA-Z \,\.\-\#\(\)]+$/', $houseName ) ) ? true : false ;
}

function isValidAddress( $address = '' ){
	return ( $address != '' && preg_match( '/^[0-9a-zA-Z \,\.\-\#\(\)]+$/', $address ) ) ? true : false ;
}

function isValidTownCity( $townCity = '' ){
	return ( $townCity != '' && preg_match( '/^[a-zA-Z \,\.\-]+$/', $townCity ) ) ? true : false ;
}

function isValidDate( $date = '', $format = "dd-mm-yyyy", $allowZero = false ){
	
	switch( $format ){
		case 'dd-mm-yyyy':
			if( $allowZero && $date == '00-00-0000' ){
				return true;
			}
			return ( $date != '' && preg_match( '/^(0[1-9]|[12][0-9]|3[01])(\/|\-|\.)(0?[1-9]|1[012])(\/|\-|\.)((19|20|21)\d\d)$/', $date ) ) ? true : false ;
		break;
		case 'dd-mm-yy':
			if( $allowZero && $date == '00-00-00' ){
				return true;
			}
			return ( $date != '' && preg_match( '/^(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/(\\d\\d)$/', $date ) ) ? true : false ;
		break;
	}
}

function isValidEmail( $email = '' ){
	return ( $email != '' && preg_match( '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/i', $email ) ) ? true : false ;
}

function isValidPostcode( $postcode = '' ){

	$alpha1 = "[ABCDEFGHIJKLMNOPRSTUWYZ]";
	$alpha2 = "[ABCDEFGHKLMNOPQRSTUVWXY]";
	$alpha3 = "[ABCDEFGHJKPMNRSTUVWXY]";
	$alpha4 = "[ABEHMNPRVWXY]";
	$alpha5 = "[ABDEFGHJLNPQRSTUWXYZ]";
	
	$pcexp[0] = '/^('.$alpha1.'{1}'.$alpha2.'{0,1}[0-9]{1,2})([[:space:]]{0,})([0-9]{1}'.$alpha5.'{2})$/';
	$pcexp[1] ='/^('.$alpha1.'{1}[0-9]{1}'.$alpha3.'{1})([[:space:]]{0,})([0-9]{1}'.$alpha5.'{2})$/';
	$pcexp[2] ='/^('.$alpha1.'{1}'.$alpha2.'{1}[0-9]{1}'.$alpha4.')([[:space:]]{0,})([0-9]{1}'.$alpha5.'{2})$/';

	$valid = false;

	foreach ( $pcexp as $regexp ) {
		if ( preg_match( $regexp, $postcode, $matches ) ) {
			$valid = true;
			break;
		}
	}

	if ( $valid ){
		return true;
	} 
	else{
		return false;
	}
}

function isValidMapCoord( $mapCoord = '' ){
	return ( $mapCoord != '' && preg_match( '/^[0-9\.\-]+$/', $mapCoord ) ) ? true : false ;
}

function isValidCopy( $copy = '' ){
	// need a more stringent validation check
	return ( $copy != '' ) ? true : false ;
}

function isValidID( $id = '' ){
	return ( $id != '' && preg_match( '/^[0-9]+$/', $id ) ) ? true : false ;
}

function isValidAction( $action = '' ){
	if( $action == '' ){
		return false;
	}
	
	$actions = array(
		'add',
		'edit',
		'delete'
	);
	
	return ( in_array($action, $actions) );
}
?>