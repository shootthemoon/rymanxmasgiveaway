<?php
/* PROJECT DEFINITIONS*/

define( 'CONTROLLERS', INCLUDES .'/controllers' );
define( 'VIEWS', INCLUDES .'/views' );
define( 'CLASSES', INCLUDES .'/classes' );
define( 'LIBRARIES', INCLUDES .'/libraries' );


define( 'LAYOUTS', INCLUDES .'/layouts' );

define( 'ELEMENTS', INCLUDES .'/elements' );
