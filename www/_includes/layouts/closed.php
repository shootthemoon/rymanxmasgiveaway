<?php include( ELEMENTS . '/html_header.php' ); ?> 
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<div class="logo-clear">
			<div class="inner">
				<img src="/images/ryman_logo.png" alt="Ryman Stationary">
			</div>
		</div>
        <header>
            <div class="inner header">
                <img class="titles" src="/images/header-title.png" alt="120 Years celebration prize draw">
                <img class="titles" src="/images/header-subtitle.png" alt="Winner announced christmas eve!">
                <p>We are counting down to celebrating our birthday with the biggest prize draw we have ever done. We are giving away 120 PRIZES in total worth over &pound;4,000!</p>
                <img class="header-stars" src="/images/header-stars.png" alt="header-stars">
            </div>
        </header>
        <section>
            <div class="inner formbody">
                <img class="prods" src="/images/products.png" alt="Products">
                <div id="app-form">
                
	                <h2>HOW TO ENTER the 120 Years Celebration Prize Draw</h2>
	                <p class="enter-closed">This competition is now closed, winners will be announced Christmas Eve.<br/>Please visit <a href="http://www.ryman.co.uk/">http://www.ryman.co.uk/</a> for more great offers.</p>
	                    
            	</div>
                <img class="stars-bottom" src="/images/form-stars.png" alt="Stars">
            </div>
            <p style="color:#fff"><?php echo date(format);?></p>        </div>
</section>
<?php include( ELEMENTS . '/html_footer.php' ); ?>