<?php include( ELEMENTS . '/html_header.php' ); ?> 
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <header>
            <div class="inner header">
                <img class="titles" src="/images/header-title.png" alt="120 Years celebration prize draw">
                <img class="titles" src="/images/header-subtitle.png" alt="Winner announced christmas eve!">
                <p>We are counting down to celebrating our birthday with the biggest prize draw<br>we have ever done. We are giving away 120 PRIZES in total worth over &pound;4,000.</p>
            </div>
        </header>
        <section>
            <div class="inner formbody">
            	<img class="prods" src="/images/products.png" alt="Products">
                <h2>120 Year Celebration Prize Draw Terms and Conditions</h2>
			
				<ol>
					<li>This Prize Draw is open to all UK residents over the age of 18, excluding Ryman Limited Colleagues and their families and employees of affiliated companies.</li>
					<li>To enter you need a unique code then visit <a href="http://www.rymancountdowndraw.co.uk">www.rymancountdowndraw.co.uk</a>, you will be required to enter contact details and your unique code.  Unique codes can be obtained in one of the following ways.<br/>
						<ol>
							<li>From the 30th November, Like us on our <a href="https://a.pgtb.me/7wftjJ">Facebook Prize Draw Page</a> and we&#39;ll email you with your unique entry code</li>
							<li>From the 2nd &ndash; 13th December 2013 inclusive, &#39;like us&#39; at our <a href="https://www.facebook.com/rymanstationery">Facebook page</a> and we&#39;ll email you a code, then share one of our daily online deals</li>
							<li>From 12th December, pick up  a prize draw voucher available in all Ryman stores Nationwide</li>
						</ol>
					</li>
					<li> If you do not have a computer or a device to access the website, visit your local Ryman store and complete a hard copy entry form which will then be entered in to the draw on your behalf. No purchase is necessary.</li>
					<li>Ryman Limited will not be liable for applications not received, incomplete, or incorrect entries, or for entries delayed or damaged. Last date for receipt of completed entries via <a href="http://www.rymancountdowndraw.co.uk">www.rymancountdowndraw.co.uk</a> is midnight 23 December 2013.</li>
					<li>The 120 years celebration prize draw is limited to one entry per person, per unique code, per email address. Multiple entries will not be entered into the final draw. No third party entries accepted.</li>
					<li>All entries received by midnight on 23rd  December 2013 will be entered into the Prize Draw. 120 prize winners will be drawn at random and as each winner is drawn prizes will be awarded in the following order ; 15 iPod Shuffle, 14 Moleskine Notebooks, 13 Beanie Boos,12 Mega Art Sets, 11 Sketching Easels, 10 Flash Drives, 9 Filofax, 8 HP Printers, 7 Compact Cameras, 6 Retro DAB radios, 5 Antique Globes, 4 Recycled Leather Satchels, 3 Cross Pens, 2 Emma Bridgewater DAB Radios, 1 DSLR Camera. The prize is as stated and cannot be transferred or exchanged. There is no cash alternative.</li>
					<li>Winners will be notified by email on 24th December 2013. In the event the claim for a prize is not received by Ryman Limited within 14 days of notification, we reserve the right to select an alternative winner. After claiming a prize winners must allow up to 28 days for delivery.  A full list of Winners Names and their counties will be available from 1st to 28th February 2014 if requested in writing from:- 120 Years Celebration Prize Draw Winners, Marketing Department, Ryman House, Savoy Road, Crewe. CW1 6NA.</li>
					<li>Ryman Limited reserve the right to amend, change or withdraw the draw at any time without notice and to substitute the prize of equal or greater value in the event of unavailability due to circumstances beyond the our control.</li>
					<li>By entering this Prize Draw, entrants agree to be bound by the rules and by any other requirements set out in the promotional material.</li>
					<li>By entering the Prize Draw, the winner consents to any publicity generated as a result of the draw and subsequent use on websites, in magazines or mobile services at any time without further consent or payment.</li>
					<li>Promoter Ryman Limited, Ryman House, Savoy Road, Crewe CW1 6NA.</li>
				</ol>

            <img src="/images/form-stars.png" alt="Stars">
        </div>
</section>

<?php include( ELEMENTS . '/html_footer.php' ); ?>