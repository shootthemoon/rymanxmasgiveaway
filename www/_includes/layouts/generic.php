<?php include( ELEMENTS . '/html_header.php' ); ?> 
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
		<div class="logo-clear">
			<div class="inner">
				<img src="/images/ryman_logo.png" alt="Ryman Stationary">
			</div>
		</div>
        <header>
            <div class="inner header">
                <img class="titles" src="/images/header-title.png" alt="120 Years celebration prize draw">
                <img class="titles" src="/images/header-subtitle.png" alt="Winner announced christmas eve!">
                <p>We are counting down to celebrating our birthday with the biggest prize draw we have ever done. We are giving away 120 PRIZES in total worth over &pound;4,000!</p>
                <img class="header-stars" src="/images/header-stars.png" alt="header-stars">
            </div>
        </header>
        <section>
            <div class="inner formbody">
                <img class="prods" src="/images/products.png" alt="Products">
                <div id="app-form">
                
	                <h2>HOW TO ENTER the 120 Years Celebration Prize Draw</h2>
	                    <p class="enter-intro">To enter you need a unique code which you can get in one of the following ways:</p>
	                    <ul class="entries">
	                    	<li>From the 30th November, &#39;like us&#39; on our <a href="https://a.pgtb.me/7wftjJ">Facebook Prize Draw Page</a> and we’ll email you with your unique entry code.</li>
	                    	<li>  From the 2nd – 13th December 2013 inclusive, &#39;like us&#39; at our <a href="https://www.facebook.com/rymanstationery">Facebook page</a> and we’ll email you a code, then share one of our daily online deals.</li>
	                    	<li>From 12th December, pick up a prize draw voucher available in all Ryman Stores Nationwide.</li>
	                    </ul>
	                    <p>Once you have your code simply enter this code together with your contact details, including a contact email address (so we can contact you if you are a winner!) to be in with a chance of winning one of our great prizes. Your entry must be received by 23.59 on 23rd December 2013.</p>
	                    <h3>Our prizes include:</h3>
	                    <div class="prod-lists">
	                        <ul>
	                            <li>15 iPod Shuffles</li>
	                            <li>14 Moleskine Notebooks</li>
	                            <li>13 Beanie Boos</li>
	                            <li>12 Mega Art Sets</li>
	                            <li>11 Sketching Easels</li>
	                        </ul>
	                        <ul>
	                            <li>10 Flash Drives</li>
	                            <li>9 Filofax</li>
	                            <li>8 HP Printers</li>
	                            <li>7 Compact Cameras</li>
	                            <li>6 Retro DAB Radios</li>
	                        </ul>
	                        <ul>
	                            <li>5 Antique Globes</li>
	                            <li>4 Recycled Leather Satchels</li>
	                            <li>3 Cross Pens</li>
	                            <li>2 Emma Bridgewater</li>
	                            <li>DAB Radios</li>
	                            <li>1 DSLR Camera</li>
	                        </ul>
	                    </div>
	                <form action="" method="post" id="entry-form" class="cf">
	        
	                <div id="entry-copy">
	                    <div id="user-message" class="msg">
	                        <h3>Oops</h3>
	                        <p>Sorry but there was a problem submitting your request.</p>
	                        <p>Please check your details and try again.</p>
	                    </div>        
	                </div>
	                <div class="form-left">
	                    <div class="field-wrap code">
	                    	<p>Enter code exactly as seen.</p>
	                        <label for="entry-code">Code:</label>
	                        <input type="text" class="text code required" id="entry-code" name="entry-code"/>
	                        <div class="field-msg"></div>
	                    </div>
	                    
	                    <div class="field-wrap title">
	                        <label for="entry-title">Title:</label>
	                        <select class="select title required" id="entry-title" name="entry-title">
	                            <option value="">---</option>
	                            <option value="mr">Mr.</option>
	                            <option value="mrs">Mrs.</option>
	                            <option value="miss">Miss.</option>
	                            <option value="ms">Ms.</option>
	                            <option value="dr">Dr.</option>
	                            <option value="rev">Rev.</option>
	                            <option value="prof">Prof.</option>
	                        </select>
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap first-name">
	                        <label for="entry-first-name">First Name:</label>
	                        <input type="text" class="text first-name required" id="entry-first-name" name="entry-first-name" />
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap last-name">
	                        <label for="entry-last-name">Surname:</label>
	                        <input type="text" class="text last-name required" id="entry-last-name" name="entry-last-name" />
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap address-1">
	                        <label for="entry-address-1">Address 1:</label>
	                        <input type="text" class="text address-1 required" id="entry-address-1" name="entry-address-1" />
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap address-2">
	                        <label for="entry-address-2">Address 2:</label>
	                        <input type="text" class="text address-2 required" id="entry-address-2" name="entry-address-2" />
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap county">
	                        <label for="entry-county">County:</label>
	                        <select class="select county required" id="entry-county" name="entry-county">
	                            <option value="">---</option>
	                            <?php 
	                            $countiesSql = "SELECT * FROM counties ORDER BY county ASC";
	                    
	                            $countiesQuery = mysql_query( $countiesSql );
	                            
	                            if( mysql_num_rows( $countiesQuery ) > 0 ){
	                                while( $county = mysql_fetch_array( $countiesQuery ) ){
	                                    echo "<option value=\"{$county['id']}\">{$county['county']}</option>";
	                                }
	                            }
	                            ?>
	                        </select>
	                    
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap postcode">
	                        <label for="entry-postcode">Postcode:</label>
	                        <input type="text" class="text postcode required" id="entry-postcode" name="entry-postcode" />
	                        <div class="field-msg"></div>
	                    </div>
	                </div>
	            
	                <div class="form-right">
	                    <div class="field-wrap telephone">
	                        <label for="entry-telephone">Telephone Number:</label>
	                        <input type="text" class="text telephone required" id="entry-telephone" name="entry-telephone" />
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap email">
	                        <label for="entry-email">Email:</label>
	                        <input type="text" class="text email required" id="entry-email" name="entry-email" />
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap dob">
	                        <label for="entry-dob">Date of Birth:</label>
	                        <select class="dob dd required" id="entry-dob" name="entry-dob[dd]" />
	                            <option value="">---</option>
	                            <?php 
	                            for( $dobDD = 1; $dobDD < 32; $dobDD ++ ){
	                                $dobDDpadded = str_pad($dobDD,2,'0',STR_PAD_LEFT);
	                                echo "<option value=\"{$dobDDpadded}\">{$dobDD}</option>";
	                            }
	                            ?>
	                        </select>
	                        <select class="dob mm required" name="entry-dob[mm]" />
	                            <option value="">---</option>
	                            <?php 
	                            for( $dobMM = 1; $dobMM < 13; $dobMM ++ ){
	                                $dobMMpadded = str_pad($dobMM,2,'0',STR_PAD_LEFT);
	                                echo "<option value=\"{$dobMMpadded}\">{$dobMM}</option>";
	                            }
	                            ?>
	                        </select>
	                        <select class="dob yy required" name="entry-dob[yy]" />
	                            <option value="">---</option>
	                            <?php 
	                            for( $dobYY = 2013; $dobYY > 1920; $dobYY -- ){
	                                echo "<option value=\"{$dobYY}\">{$dobYY}</option>";
	                            }
	                            ?>
	                        </select>
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap facebook">
	                        <label for="entry-facebook">Facebook <em>(optional)</em>:</label>
	                        <input type="text" class="text facebook" id="entry-facebook" name="entry-facebook" />
	                        <div class="field-msg"></div>
	                    </div>
	                    
	                    <div class="field-wrap">
	                        <label for="entry-twitter">Twitter <em>(optional)</em>:</label>
	                        <input type="text" class="text twitter" id="entry-twitter" name="entry-twitter" />
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap">
	                    	<p class="data-protection"><strong>Data Protection Act:</strong> By responding to this offer you also agree that your personal data may be held on computer and may be used as described. Your data will be treated in confidence and will not be disclosed to third parties. Your personal data may be shared with any company from time to time in the Ryman Group of associated companies for market research, statistical analysis and so that you can be contacted with details of special offers and promotions. Your data may also be disclosed where permitted by law or where you have given your consent. You may be contacted by mail, telephone, email or other reasonable methods with details of products or services offered by ourselves. Under the terms of the Data Protection Act 1998 you have the right to obtain a copy of the information held about you upon payment of the appropriate fee.If you prefer not to receive this information, please tick the box on the prize draw entry page.</p>
	                        <label for="entry-confirm" class="entry-confirm">Accept our privacy policy:</label>
	                        <input type="checkbox" class="checkbox confirm required" id="entry-confirm" name="entry-confirm" />
	                        <div class="field-msg"></div>
	                    </div>
	                                
	                    <div class="field-wrap">
	                        <?php echo recaptcha_get_html( CAPTCHA_PUBLIC_KEY ); ?>
	                        <span id="entry-captcha"></span>
	                        <div class="field-msg"></div>
	                    </div>
	                </div>
	            
	            
	            
	                <div class="field-wrap submit"> 
	                    <input type="submit" class="submit" id="entry-submit" name="entry-submit" />
	                </div>
	            
	            </form>
            	
            </div>
            <img class="stars-bottom" src="/images/form-stars.png" alt="Stars">
        </div>
</section>
<?php include( ELEMENTS . '/html_footer.php' ); ?>