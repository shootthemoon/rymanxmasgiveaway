<?php
require_once( INCLUDES . '/config.php' );

#$_POST['entry-code'] = $_GET['code'];

if( !empty( $_POST ) && isset( $_GET['type'] ) && $_GET['type'] == 'entry' ){
	$data = new stdClass();
	
	$formValid = true;
	
	$competitionEntry = new Entry();
	
	$fieldErrors = array();
	
	// validate entry code
	if( isset( $_POST['entry-code'] ) ){
		$code = new Code( $_POST['entry-code'] );
		
		if( !$code->isCodeValid() || $code->hasCodeBeenRedeemed() ){
			$fieldErrors['entry-code'] = $code->getError();
			
			if( $fieldErrors['entry-code'] == 'Code supplied has already been redeemed' ){
				$data->voucherRedeemed = true;
			}
		}
	}
	else{
		$fieldErrors['entry-code'] = 'No code supplied';
	}
	
	
	// validate title
	if( isset( $_POST['entry-title'] ) ){
	
		if( !isValidTitle( $_POST['entry-title'] ) ){
			$fieldErrors['entry-title'] = 'Invalid title selected';
		}
	}
	else{
		$fieldErrors['entry-title'] = 'No title selected';
	}
	
	
	// validate first name
	if( isset( $_POST['entry-first-name'] ) && $_POST['entry-first-name'] != '' ){
	
		if( !isValidName( $_POST['entry-first-name'] ) ){
			$fieldErrors['entry-first-name'] = 'Invalid characters in your firstname. Please use only letters, spaces, hyphens and apostrophes';
		}
	}
	else{
		$fieldErrors['entry-first-name'] = 'No firstname supplied';
	}
	
	// validate last name
	if( isset( $_POST['entry-last-name'] ) && $_POST['entry-last-name'] != '' ){
	
		if( !isValidName( $_POST['entry-last-name'] ) ){
			$fieldErrors['entry-last-name'] = 'Invalid characters in your surname. Please use only letters, spaces, hyphens and apostrophes';
		}
	}
	else{
		$fieldErrors['entry-last-name'] = 'No surname supplied';
	}
	
// validate address 1
	if( isset( $_POST['entry-address-1'] ) && $_POST['entry-address-1'] != '' ){
	
		if( !isValidAddress( $_POST['entry-address-1'] ) ){
			$fieldErrors['entry-address-1'] = 'Invalid characters in the first line of your address. Please use only numbers, letters, spaces and basic characters (i.e. fullstops, commas, hyphens, brackets and the hash symbol.';
		}
	}
	else{
		$fieldErrors['entry-address-1'] = 'No first line of address supplied';
	}
	
	// validate address 2
	if( isset( $_POST['entry-address-2'] ) && $_POST['entry-address-2'] != '' ){
	
		if( !isValidAddress( $_POST['entry-address-2'] ) ){
			$fieldErrors['entry-address-2'] = 'Invalid characters in the second line of your address. Please use only numbers, letters, spaces and basic characters (i.e. fullstops, commas, hyphens, brackets and the hash symbol.';
		}
	}
	
	// validate postcode
	if( isset( $_POST['entry-county'] ) && $_POST['entry-county'] != '' ){
		if( !isValidID( $_POST['entry-county'] ) ){
			$fieldErrors['entry-county'] = 'The supplied county is incorrect.';
		}
	}
	else{
		$fieldErrors['entry-county'] = 'No county selected';
	}
	
	// validate postcode
	if( isset( $_POST['entry-postcode'] ) && $_POST['entry-postcode'] != '' ){
		$_POST['entry-postcode'] = strtoupper( str_replace(' ', '', $_POST['entry-postcode'] ) );
		
		if( !isValidPostcode( $_POST['entry-postcode'] ) ){
			$fieldErrors['entry-postcode'] = 'The supplied postcode is incorrect.';
		}
	}
	else{
		$fieldErrors['entry-postcode'] = 'No postcode supplied';
	}
	
	// validate telephone
	if( isset( $_POST['entry-telephone'] ) && $_POST['entry-telephone'] != '' ){
	
		if( !isValidPhone( $_POST['entry-telephone'] ) ){
			$fieldErrors['entry-telephone'] = 'The supplied telephone number can ony contain the digits 0-9, spaces, brackets or the + - symbols.';
		}
	}
	else{
		$fieldErrors['entry-telephone'] = 'No telephone supplied';
	}
	
	
// validate email
	if( isset( $_POST['entry-email'] ) && $_POST['entry-email'] != '' ){
	
		if( !isValidEmail( $_POST['entry-email'] ) ){
			$fieldErrors['entry-email'] = 'The supplied email address doesn\'t appear to be correct.';
		}
		
		if( $competitionEntry->hasEmailAddressBeenUsedBefore( $_POST['entry-email'] ) ){
			$fieldErrors['entry-email'] = 'The supplied email address is already linked to an entry for this competition.';
		}
		
	}
	else{
		$fieldErrors['entry-email'] = 'No email address supplied.';
	}
	
	// validate dob
	if( isset( $_POST['entry-dob'] ) && !empty( $_POST['entry-dob'] ) ){
	
		$dob = "{$_POST['entry-dob']['dd']}-{$_POST['entry-dob']['mm']}-{$_POST['entry-dob']['yy']}";
		
		if( !isValidDate( $dob ) ){
			$fieldErrors['entry-dob'] = 'Your date of birth doesn\'t appear to be correct.';
		}
	}
	else{
		$fieldErrors['entry-dob'] = 'No date of birth supplied';
	}
	
	
	/* validate CAPTCHA */
	if( isset( $_POST["recaptcha_response_field"] ) ){
		$captchaResponse = recaptcha_check_answer (
			CAPTCHA_PRIVATE_KEY,
			$_SERVER["REMOTE_ADDR"],
			$_POST["recaptcha_challenge_field"],
			$_POST["recaptcha_response_field"]
		);
	
		if ( !$captchaResponse->is_valid ) {
			$fieldErrors['entry-captcha'] = $error = $captchaResponse->error;
		}
	}

	
	
	// validate confirmation
	if( !isset( $_POST['entry-confirm'] ) || $_POST['entry-confirm'] != 'on' ){
		$fieldErrors['entry-confirm'] = 'You need to accept that you have read our policy.';
	}

	
	if( count( $fieldErrors ) > 0 ){
		$formValid = false;
		
		$data->formValid = false;
		$data->errors = $fieldErrors;
	}
	else{
		$data->formValid = true;
		
		$code->redeemCode();
		
		$competitionEntry->saveEntry( $_POST, $code->getCodeID() );
	}
	
	echo json_encode( $data );
}
elseif( !empty( $_POST ) && isset( $_GET['type'] ) && $_GET['type'] == 'error' ){
	$data = new stdClass();
	
	$email = array();
	
	
	$email['to'] .= "prizedraw@ryman.co.uk" . ', ';
	$email['to'] .= "kathryn@shoot-the-moon.co.uk";
	$email['subject']  = "Competition entry - Error";
	$email['message']  = "There was an competition entry online and the code submitted has been previously used.\n\n";
	$email['message'] .= "ENTRY DETAILS\n===================\n\n";
	$email['message'] .= "USER: {$_POST['entry-title']} {$_POST['entry-first-name']} {$_POST['entry-last-name']}\n";
	$email['message'] .= "ADDRESS: {$_POST['entry-address-1']}, {$_POST['entry-address-2']}, {$_POST['entry-county']}, {$_POST['entry-postcode']}\n";
	$email['message'] .= "PHONE: {$_POST['entry-telephone']}\n";
	$email['message'] .= "EMAIL: {$_POST['entry-email']}\n";
	$email['message'] .= "CODE: {$_POST['entry-code']}\n";
	$email['from'] = "info@rymanxmasgiveaway.co.uk";
	$email['headers'] = "From:" . $email['from'];
	
	mail($email['to'],$email['subject'],$email['message'],$email['headers']);
	
	$data->response = "Email has been sent";
	
	echo json_encode($data);
}
else{
	die('Access forbidden');
}

