<?php
// LOAD ANY DEFINITIONS
require_once( INCLUDES . '/definitions.php' );

// ESTABLISH OUR DB CONNECTION
require_once( INCLUDES . '/db.php' );

// INCLUDE ANY VALIDATIONS WE WILL BE USING
require_once( INCLUDES . '/validations.php' );

/*
 * autoload our core classes
*/
$classesDir = opendir( CLASSES . '/' );

if ( $classesDir ) {
	while ( false !== ( $fileName = readdir( $classesDir ) ) ) {
		if ( ( $fileName != '.' ) && ( $fileName != '..' ) ) {
			require_once( CLASSES .'/' . $fileName );
		}
	}
	closedir( $classesDir );
}


/*
 * autoload our core libraries
*/
$libraryDir = opendir( LIBRARIES . '/' );

if ( $libraryDir ) {
	while ( false !== ( $fileName = readdir( $libraryDir ) ) ) {
		if ( ( $fileName != '.' ) && ( $fileName != '..' ) ) {
			require_once( LIBRARIES .'/' . $fileName );
		}
	}
	closedir( $libraryDir );
}


/* HANDLE ANY ENVIRONMENT CHANGES */

switch( APP_ENVIRONMENT ){
	case 'DEVELOPMENT':
		define( 'CAPTCHA_PUBLIC_KEY', '6LeU5eoSAAAAAGvtOvKGtcev1jR1CbwQ5Hb_-icl' );
		define( 'CAPTCHA_PRIVATE_KEY', '6LeU5eoSAAAAAHxeL8dV8cy9yVV4x6mLCxYWQ723' );
	break;
	
	case 'STAGING':
		define( 'CAPTCHA_PUBLIC_KEY', '6LeH4-oSAAAAAMXbXG76683WBDo0nRmazOAHbcrp' );
		define( 'CAPTCHA_PRIVATE_KEY', '6LeH4-oSAAAAAFqWv7zgVlMB_aGaHzPt3wVFYESQ' );
	break;
		
	case 'PRODUCTION':
		define( 'CAPTCHA_PUBLIC_KEY', '6LeV5eoSAAAAACHZR8IGKLtDQvmvJenK8jxR_gTF' );
		define( 'CAPTCHA_PRIVATE_KEY', '6LeV5eoSAAAAAEUP5ORdGjx63LUk2BmOssMPRMIM' );
	break;
}
