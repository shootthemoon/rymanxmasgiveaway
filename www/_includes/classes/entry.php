<?php
Class Entry{
	
	private $entryID;
	private $entryData;
	
	private $_errors = array();
	
	function __construct(){
		
	}
	
	function saveEntry( $entryData = array(), $entryCode = '' ){
		
		if( empty( $entryData ) ){
			$this->_errors[] = 'No entry data supplied';
			return false;
		}
		
		if( $entryCode == '' ){
			$this->_errors[] = 'No entry code supplied';
			return false;
		}
		
		foreach( $entryData as $key => $value ){
			
			if( !is_array( $value ) ){
				$entryData[$key] = mysql_real_escape_string( $value );
			}
		}
		
		$entryDataDOB = "{$entryData['entry-dob']['yy']}-{$entryData['entry-dob']['mm']}-{$entryData['entry-dob']['dd']}";
		
		$entryTitle = strtoupper( $entryData['entry-title'] );
		
		$sql = "
			INSERT INTO
				`entries` (
					`time`,
					`user_ip`,
					`user_email`,
					`user_title`,
					`user_first_name`,
					`user_last_name`,
					`user_address_1`,
					`user_address_2`,
					`user_county_id`,
					`user_postcode`,
					`user_telephone`,
					`user_dob`,
					`user_facebook`,
					`user_twitter`,
					`cid`,
					`status`
				)
			VALUES (
				NOW(),
				'{$_SERVER['REMOTE_ADDR']}',
				'{$entryData['entry-email']}',
				'{$entryTitle}',
				'{$entryData['entry-first-name']}',
				'{$entryData['entry-last-name']}',
				'{$entryData['entry-address-1']}',
				'{$entryData['entry-address-2']}',
				'{$entryData['entry-county']}',
				'{$entryData['entry-postcode']}',
				'{$entryData['entry-telephone']}',
				'{$entryDataDOB}',
				'{$entryData['entry-facebook']}',
				'{$entryData['entry-twitter']}',
				'{$entryCode}',
				'SUCCESS'
			)
		";

		$query = mysql_query( $sql );

		if( $query ){
			return mysql_insert_id();
		}
		else {
			return false;
		}
		
		return false;	
	}
	
	
	function hasEmailAddressBeenUsedBefore( $emailAddress ){
		
		$sql = "
			SELECT
				id
			FROM
				`entries`
			WHERE
				`user_email` = '{$emailAddress}'
			LIMIT 1
		";
		
		$query = mysql_query( $sql );
		
		return( mysql_num_rows( $query ) > 0 ) ? true : false ;
	}
	
	
	function getErrors(){
		return ( !empty( $this->_errors ) ) ? $this->_errors : false ;
	}
	
}