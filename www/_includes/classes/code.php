<?php
Class Code{
	
	private $_competitionCode;
	
	private $_codeValid    = false;
	private $_codeRedeemed = false;
	private $_codeID;
	
	private $_error = '';
	
	function __construct( $suppliedCode = '' ){
		if( $suppliedCode == '' ){
			$this->_error = "No code supplied";
			return $this;
		}
		
		if( !preg_match( '/^[A-Z]{3}\d{3}$/', $suppliedCode ) ){
			$this->_error = "Supplied code failed validation";
			return $this;
		}
		
		$this->_competitionCode = $suppliedCode;
		
		$sql = "
			SELECT
				*
			FROM
				`codes`
			WHERE
				`code` = '{$this->_competitionCode}'
			LIMIT 1
		";
		
		$query = mysql_query( $sql );
		
		if( mysql_num_rows( $query ) > 0 ){
			$row = mysql_fetch_assoc( $query );
			
			$this->_codeID = $row['id'];
			
			if( $row['redeemed'] != 'Y' ){
				// VALID CODE, NOT REDEEMED
				$this->_codeValid = true;
				$this->_codeRedeemed = false;
			}
			else{
				// VALID CODE, ALREADY REDEEMED]
				$this->_codeValid = true;
				$this->_codeRedeemed = true;
				
				$this->_error = "Code supplied has already been redeemed";
			}
		}
		else{
			$this->_codeValid = false;
			$this->_codeRedeemed = false;
			
			$this->_error = "Code supplied isn't valid";
		}
		
		return $this;
	}
	
	function redeemCode(){
		// update the code to show it's been used
		
		$sql = "
			UPDATE
				`codes`
			SET
				`redeemed` = 'Y'
			WHERE
				id = '{$this->_codeID}'
		";
		
		$query = mysql_query( $sql );
		
		if( mysql_affected_rows() > 0 ){
			return true;
		}
		else {
			return ( !mysql_error() ) ? true : false ;
		}
	}
	
	function isCodeValid(){
		return $this->_codeValid;
	}
	
	function getCodeID(){
		return $this->_codeID;
	}
	
	function hasCodeBeenRedeemed(){
		return $this->_codeRedeemed;
	}
	
	function getError(){
		return ( $this->_error != '' ) ? $this->_error : false ;
	}
}